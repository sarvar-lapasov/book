import axios from "./axios";

export default {
    actions: {
        fetchCategories(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/categories')
                    .then((response) => {
                        console.log('kategoriyalar muvaffaqiyatli olindi')

                        let categories = response.data['hydra:member'];
                        context.commit('updateCategories', categories)
                        resolve()

                    })
                    .catch(() => {
                        console.log('kategoriyalar olishda xatolik')
                        reject()
                    })

            })

        }
    },
    mutations: {
        updateCategories(state, categories) {
            state.categories = categories
            localStorage.setItem("c", JSON.stringify(categories))
        }
    },
    state: {
        categories: JSON.parse(localStorage.getItem('c'))
    },
    getters: {
        getCategories(state) {
            return state.categories
        }
    }
}