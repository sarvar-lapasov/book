import axios from "axios";

export default {
    actions: {
        fetchUserToken(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/users/auth", data)
                    .then((response) => {
                        console.log("token muvaffaqiyatli olindi");
                        console.log(response.data);
                        console.log(data);
                        console.log(data.email + "  data ketdi");
                        context.commit(
                            "updateToken",
                            response.data.accessToken
                        );
                        resolve();
                    })
                    .catch(() => {
                        console.log("token olishda xatolik");
                        reject();
                    })
                    .finally(() => {
                        console.log("oxirgi bolib finally() ishladi");
                    });
            });
        },
        aboutMe(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/users/about_me", data)
                    .then((response) => {
                        console.log("token muvaffaqiyatli olindi");
                        console.log(response.data);
                        console.log(data.email + "  data ketdi");
                        context.commit("updateUser", response.data);
                        resolve();
                    })
                    .catch(() => {
                        console.log("data olishda xatolik");
                        reject();
                    })
                    .finally(() => {
                        console.log("oxirgi bolib finally() ishladi");
                    });
            });
        },
        register(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/users", data)
                    .then((response) => {
                        console.log("register muvaffaqiyatli");
                        console.log(response.data);
                        resolve();
                    })
                    .catch(() => {
                        console.log("registerda xatolik");
                        reject();
                    })
                    .finally(() => {
                        console.log("oxirgi bolib finally() ishladi");
                    });
            });
        },
    },
    mutations: {
        updateToken(state, token) {
            state.token = token;
            localStorage.setItem("token", token);
        },
        updateUser(state, user) {
            state.user = user;
            localStorage.setItem("user", JSON.stringify(user));
        },
    },
    state: {
        token: localStorage.getItem("token"),
        user: localStorage.getItem("user") ? JSON.parse(localStorage.getItem('user')) : "",
    },
    getters: {
        getToken(state) {
            return state.token;
        },
        getUser(state) {
            return state.user;
        },
    },
};
