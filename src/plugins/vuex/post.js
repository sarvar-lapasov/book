import axios from "./axios";

export default {
    actions: {
        fetchPosts(context, data) {
            if (!data.page) {
                data.page = 1;
            }
            let url = "?page=" + data.page;

            return new Promise((resolve, reject) => {
                axios
                    .get("/posts" + url + "&order[id]=desc")
                    .then((response) => {
                        console.log("postlar muvaffaqiyatli olindi");
                        console.log(response);
                        let posts = {
                            models: response.data["hydra:member"],
                            totalItems: response.data["hydra:totalItems"],
                        };
                        console.log(posts.models);
                        context.commit("updatePosts", posts);
                        resolve();
                    })
                    .catch(() => {
                        console.log("postlar olishda xatolik");
                        reject();
                    });
            });
        },
        fetchPost(context, postId) {
            return new Promise((resolve, reject) => {
                axios
                    .get("/posts/" + postId)
                    .then((response) => {
                        console.log("postlar muvaffaqiyatli olindi");
                        console.log(response);
                        context.commit("updatePost", response.data);
                        resolve();
                    })
                    .catch(() => {
                        console.log("postlar olishda xatolik");
                        reject();
                    });
            });
        },
        deletePost(context, postId) {
            return new Promise((resolve, reject) => {
                axios
                    .delete("/posts/" + postId)
                    .then((response) => {
                        console.log("post ochirildi");
                        console.log(response);
                        resolve();
                    })
                    .catch(() => {
                        console.log("postlar ochirishda xatolik");
                        reject();
                    });
            });
        },
        pushPost(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/posts", data)
                    .then((response) => {
                        console.log("yangi post bazaga qoshildi");
                        console.log(response.data);
                        console.log(response);
                        console.log(data);
                        resolve();
                    })
                    .catch(() => {
                        console.log("yangi post bazaga qoshilmadi");
                        reject();
                        console.log(data);
                    })
                    .finally(() => {
                        console.log("oxirgi bolib finally() ishladi");
                    });
            });
        },
        editPost(context, { id, data }) {
            return new Promise((resolve, reject) => {
                axios
                    .put("/posts/" + id, data)
                    .then((response) => {
                        console.log(" post o'zgartirildi");
                        console.log(response.data);
                        console.log(response);
                        console.log(data);
                        resolve();
                    })
                    .catch(() => {
                        console.log("post malumotlari o'zgarmadi");
                        reject();
                    })
                    .finally(() => {
                        console.log("oxirgi bolib finally() ishladi");
                    });
            });
        },
    },
    mutations: {
        updatePosts(state, posts) {
            state.posts = posts;
        },
        updatePost(state, post) {
            state.post = post;
            localStorage.setItem('post', JSON.stringify(post))
        },
    },
    state: {
        posts: {
            models: [],
            totalItems: 0,
        },
        post: {
            title: '',
            description: '',
            content: '',
            createdAt: '',
            picture: '',
            user: {
                firstName: '',
                avatar: ''
            }
        }
    },
    getters: {
        getPosts(state) {
            return state.posts.models;
        },
        getPost(state) {
            return state.post;
        },
        getPostTotal(state) {
            return state.posts.totalItems;
        },
    },
};