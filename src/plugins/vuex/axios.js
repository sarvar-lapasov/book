import axios from "axios";
import store from "@/plugins/vuex/store";

axios.defaults.headers.common["Content-Type"] = "application/ld+json";

axios.interceptors.request.use((config) => {
    if (config.url !== "https://sarvar.ibrohimbek.uz/api/users/auth") {
        config.headers.Authorization = "bearer " + store.getters.getToken;
    }
    return config;
});

axios.defaults.baseURL = "https://sarvar.ibrohimbek.uz/api";

export default axios;
