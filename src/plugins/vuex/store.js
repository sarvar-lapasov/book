import { createStore } from "vuex";
import user from "@/plugins/vuex/user";
import book from "@/plugins/vuex/book";
import media from "@/plugins/vuex/media";
import categories from "@/plugins/vuex/categories";
import post from "@/plugins/vuex/post";

export default createStore({
    modules: {
        book,
        categories,
        media,
        post,
        user,
    },
});
