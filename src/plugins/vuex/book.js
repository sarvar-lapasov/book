import axios from "./axios";

export default {
    actions: {
        fetchBooks(context, data) {
            if (!data.page) {
                data.page = 1;
            }
            let url = "?page=" + data.page;

            // if (data.name !== null){
            //     url += '&name=' + data.name
            // }

            if (data.categoryId !== null) {
                url += "&category=" + data.categoryId;
            }

            return new Promise((resolve, reject) => {
                axios
                    .get("/books" + url + "&order[id]=desc")
                    .then((response) => {
                        console.log("kitoblar muvaffaqiyatli olindi");
                        console.log(response);
                        let books = {
                            models: response.data["hydra:member"],
                            totalItems: response.data["hydra:totalItems"],
                        };
                        console.log(books.models);
                        context.commit("updateBooks", books);
                        resolve();
                    })
                    .catch(() => {
                        console.log("kitoblar olishda xatolik");
                        reject();
                    });
            });
        },
        fetchBook(context, bookId) {
            return new Promise((resolve, reject) => {
                axios
                    .get("/books/" + bookId)
                    .then((response) => {
                        console.log("kitoblar muvaffaqiyatli olindi");
                        console.log(response);
                        context.commit("updateBook", response.data);
                        resolve();
                    })
                    .catch(() => {
                        console.log("kitoblar olishda xatolik");
                        reject();
                    });
            });
        },
        deleteBook(context, bookId) {
            return new Promise((resolve, reject) => {
                axios
                    .delete("/books/" + bookId)
                    .then((response) => {
                        console.log("kitob ochirildi");
                        console.log(response);
                        resolve();
                    })
                    .catch(() => {
                        console.log("kitoblar ochirishda xatolik");
                        reject();
                    });
            });
        },
        pushBook(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post("/books", data)
                    .then((response) => {
                        console.log("yangi kitob bazaga qoshildi");
                        console.log(response.data);
                        console.log(response);
                        console.log(data);
                        resolve();
                    })
                    .catch(() => {
                        console.log("yangi kitob bazaga qoshilmadi");
                        reject();
                    })
                    .finally(() => {
                        console.log("oxirgi bolib finally() ishladi");
                    });
            });
        },
        editBook(context, { id, data }) {
            return new Promise((resolve, reject) => {
                axios
                    .put("/books/" + id, data)
                    .then((response) => {
                        console.log(" kitob o'zgartirildi");
                        console.log(response.data);
                        console.log(response);
                        resolve();
                    })
                    .catch(() => {
                        console.log("kitob malumotlari o'zgarmadi");
                        reject();
                    })
                    .finally(() => {
                        console.log("oxirgi bolib finally() ishladi");
                    });
            });
        },
    },
    mutations: {
        updateBooks(state, books) {
            state.books = books;
        },
        updateBook(state, book) {
            state.book = book;
        },
    },
    state: {
        books: {
            models: [],
            totalItems: 0,
        },
        book: {
            name: "",
            description: "",
            text: "",
            category: "",
            picture: "",
        },
    },
    getters: {
        getBooks(state) {
            return state.books.models;
        },
        getBook(state) {
            return state.book;
        },
        getBookTotal(state) {
            return state.books.totalItems;
        },
    },
};